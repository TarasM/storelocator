<?php

namespace StoreLocator\Shop\Controller\Adminhtml\Shop;

use StoreLocator\Shop\Api\ShopRepositoryInterface;
use Magento\Backend\App\Action;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'StoreLocator_Shop::shop';

    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param ShopRepositoryInterface $shopRepository
     */
    public function __construct(
        Action\Context $context,
        ShopRepositoryInterface $shopRepository
    ) {
        parent::__construct($context);
        $this->shopRepository = $shopRepository;
    }

    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();

        try {
            $shopId = $this->getRequest()->getParam("shop_id");
            $this->shopRepository->deleteById((int)$shopId);
            $this->messageManager->addSuccessMessage("Shop was deleted");
        } catch (\Exception $ex) {
            $this->messageManager->addErrorMessage("Error on deleting shop");
        }

        return $redirect->setPath("*/*/");
    }
}
