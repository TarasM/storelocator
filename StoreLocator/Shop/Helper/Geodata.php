<?php

namespace StoreLocator\Shop\Helper;

use StoreLocator\Shop\Helper\Data;

class Geodata
{
    /**
     * @var Data $heslper
     */
    private $helper;

    public function __construct(
        Data $helper
    ) {
        $this->helper = $helper;
    }

    public function getGeodata($address)
    {
        for ($i=0; $i< strlen($address); $i++) {
            if ($address[$i] == " ") {
                $address[$i] = '+';
            }
        }

        $data = $this->helper;

        $key = $data->getGoogleMapsApiKey();

        $data = "https://maps.google.com/maps/api/geocode/json?address=$address&key=$key";
        $result = json_decode(file_get_contents($data), true);
        return $result;
    }
}

//AIzaSyABevQr90EOlMU2iMdSWs0PzHUsOKKbb20
