<?php

namespace StoreLocator\Shop\Console;

use StoreLocator\Shop\Api\ShopRepositoryInterface;
use StoreLocator\Shop\Model\Shop;
use StoreLocator\Shop\Model\ShopFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportShopsCommand extends command
{

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'StoreLocator_Shop::shop';

    /**
     * var Shop $shop
     */
    //private $shop;

    /**
     * @var  ShopRepositoryInterface $shopRepository
     */
    private $shopRepository;

    /**
     * @var ShopFactory
     */
    private $shopFactory;

    /**
     * ImportShopsCommand constructor.
     * param Shop $shop
     * @param ShopRepositoryInterface $shopRepository
     * @param ShopFactory $shopFactory
     * @param string|null $name
     */
    public function __construct(
        //Shop $shop,
        ShopRepositoryInterface $shopRepository,
        ShopFactory $shopFactory,
        string $name = null
    ) {
        //$this->shop = $shop;
        $this->shopRepository = $shopRepository;
        $this->shopFactory = $shopFactory;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName("shops:import");
        $this->addArgument('path', InputArgument::REQUIRED, __('Type a path to file'));
        $this->setDescription('import shops from file');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        $file = fopen($path, "r")or die('cannot read file');

        while ($data = fgetcsv($file, 0, '"')) {
            $arr[] = $data;
        }

        for ($i = 1; $i < count($arr); $i++) {
            $exploded = explode(",", $arr[$i][0]);
            $id = $exploded[0];
            $name = $exploded[1];
            $address = $exploded[2] . " " . $exploded[3] . " " . $exploded[5] . " " . $exploded[7];
            $postalCode = $exploded[4];
            $telNumber = $exploded[9];


            $shop = $this->shopFactory->create();
            //$shop =$this->shop;
            //$shop->setId($id);
            $shop->setName($name);
            $shop->setAddress($address);
            $shop->setDescription('Tel. : ' . $telNumber . " " . 'Postal code :' . $postalCode);
            $shop->setImagePath('http://tarasm/pub/media/storelocator/feature/no-image.png');
            $output->writeln($shop->getId());
            $this->shopRepository->save($shop);
            $output->writeln($name . ' shop was saved');
        }
    }
}
