<?php

namespace StoreLocator\Shop\Api;

use Magento\UrlRewrite\Model\UrlRewrite;

interface ShopUrlRewriteRepositoryInterface
{
    /**
     * Save rewrite.
     *
     * @param UrlRewrite $shopUrl
     * @return UrlRewrite
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(UrlRewrite $shopUrl);
}
