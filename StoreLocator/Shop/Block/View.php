<?php

declare(strict_types=1);
namespace StoreLocator\Shop\Block;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use StoreLocator\Shop\Api\Data\ShopInterface;
use StoreLocator\Shop\Api\ShopRepositoryInterface;

class View extends Template
{
    /**
     * @var ShopRepositoryInterface
     */
    protected $shopRepository;

    /**
     * View constructor.
     * @param ShopRepositoryInterface $shopRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        ShopRepositoryInterface $shopRepository,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->shopRepository = $shopRepository;
    }

    /**
     * @param null $id
     * @return ShopInterface|null
     */
    public function getShop($id = null) :?ShopInterface
    {
        if ($id) {
            try {
                /** @var \StoreLocator\Shop\Model\Shop $shop */
                $shop = $this->shopRepository->getById($id);
                return $shop;
            } catch (NoSuchEntityException $ex) {
                return null;
            }
        }
        return null;
    }
}
