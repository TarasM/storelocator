<?php

namespace StoreLocator\Shop\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    const XML_PATH_IS_ENABLED = "storelocator/shop/enabled";
    const XML_PATH_GOOGLE_MAPS_API_KEY = "storelocator/shop/google_maps_api_key";

    /**
     * Data constructor.
     * @param ScopeConfigInterface $scopeConfig
     */

    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    public function isEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_IS_ENABLED);
    }

    public function getGoogleMapsApiKey()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_GOOGLE_MAPS_API_KEY);
    }
}
