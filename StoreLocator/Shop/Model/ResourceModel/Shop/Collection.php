<?php


namespace StoreLocator\Shop\Model\ResourceModel\Shop;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package StoreLocator\Shop\Model\ResourceModel\Shop
 */
class Collection extends AbstractCollection
{
    public function _construct() //phpcs:ignore Magento2.CodeAnalysis.EmptyBlock
    {
        parent::_init(
            \StoreLocator\Shop\Model\Shop::class,
            \StoreLocator\Shop\Model\ResourceModel\Shop::class
        );
    }
}
