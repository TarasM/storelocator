<?php

namespace StoreLocator\Shop\Plugin;


use Magento\CatalogUrlRewrite\Model\Product\CurrentUrlRewritesRegenerator;
use Magento\Framework\Exception\CouldNotSaveException;

use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use StoreLocator\Shop\Helper\Geodata;
use StoreLocator\Shop\Model\ShopUrlRewriteRepository;

class ShopPlugin
{
    /**
     * @var CurrentUrlRewritesRegenerator
     */
    private $urlRepository;

    /**
     *  @var UrlRewriteFactory $urlRewriteFactory
     */
    private $urlRewriteFactory;

    /**
     * @var Geodata $geodata
     */
    private $geodata;

    /**
     * @var ShopUrlRewriteRepository
     */
    private $repository;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * ShopPlugin constructor.
     * @param CurrentUrlRewritesRegenerator $urlRepository
     * @param UrlRewriteFactory $urlRewriteFactory
     * @param Geodata $geodata
     * @param ShopUrlRewriteRepository $repository
     * @param UrlPersistInterface $urlPersist
     */
    public function __construct(
        CurrentUrlRewritesRegenerator $urlRepository,
        UrlRewriteFactory $urlRewriteFactory,
        Geodata $geodata,
        ShopUrlRewriteRepository $repository,
        UrlPersistInterface $urlPersist
    ) {
        $this->urlRepository = $urlRepository;
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->geodata = $geodata;
        $this->repository = $repository;
        $this->urlPersist = $urlPersist;
    }

    public function beforeSave(
        \StoreLocator\Shop\Model\ShopRepository $shopRepository,
        $shop
    ) {
        $address = $shop->getAddress();
        $geodata = $this->geodata->getGeodata($address);

        if (!$shop->getLatitude()) {
            $lattitude = ($geodata["results"][0]["geometry"]["location"]["lat"]);
            $shop->setLatitude($lattitude);
        }

        if (!$shop->getLongitude()) {
            $longitude = ($geodata["results"][0]["geometry"]["location"]["lng"]);
            $shop->setLongitude($longitude);
        }
    }

    public function afterSave(
        \StoreLocator\Shop\Model\ShopRepository $shopRepository,
        $shop
    ) {
        try {
            $name = $shop->getName();
            //replace ' ' with '_' in shop name for url creating
            for ($i=0; $i< strlen($name); $i++) {
                if ($name[$i] == " ") {
                    $name[$i] = '_';
                }
            }
            $id = $shop->getId();

            $urlRewriteFactory = $this->urlRewriteFactory;
            $urlRewriteModel = $urlRewriteFactory->create();

            $urlRewriteModel->setEntityType('custom');
            $urlRewriteModel->setStoreId(1);

            $targetPath = "shop/index/view/id/" . $id;
            $urlRewriteModel->setTargetPath($targetPath);
            $data[UrlRewrite::TARGET_PATH] = $targetPath;
            if (array_key_exists(UrlRewrite::TARGET_PATH, $data)) {
                $this->urlPersist->deleteByData(
                    [
                        UrlRewrite::TARGET_PATH => $targetPath
                    ]
                );
            }

            $identifier = $shop->getIdentifier();
            if (!$identifier) {
                $urlRewriteModel->setRequestPath("shop/" . $name . $id . ".html");
            } else {
                $urlRewriteModel->setRequestPath("shop/" . $identifier . $id . ".html");
            }

            $this->repository->save($urlRewriteModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
    }

    public function beforeDelete(
        \StoreLocator\Shop\Model\ShopRepository $shopRepository,
        $shop
    ) {
        try {
            $name = $shop->getName();
            //replace ' ' with '_' in shop name for url creating
            for ($i=0; $i< strlen($name); $i++) {
                if ($name[$i] == " ") {
                    $name[$i] = '_';
                }
            }
            $id = $shop->getId();
            $targetPath = "shop/index/view/id/" . $id;

            $this->urlPersist->deleteByData(
                [
                    UrlRewrite::TARGET_PATH => $targetPath
                ]
            );
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
    }
}
