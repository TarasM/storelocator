<?php

namespace StoreLocator\Shop\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewrite as ResourceUrl;
use Magento\UrlRewrite\Model\UrlRewrite;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use StoreLocator\Shop\Api\ShopUrlRewriteRepositoryInterface;

class ShopUrlRewriteRepository implements ShopUrlRewriteRepositoryInterface
{

    /**
     * @var ResourceUrl
     */
    private $resource;

    /**
     * @var UrlRewriteFactory $urlRewriteFactory
     */
    private $urlRewriteFactory;

    /**
     * ShopUrlRewriteRepository constructor.
     * @param ResourceUrl $resource
     * @param UrlRewriteFactory $urlRewriteFactory
     */
    public function __construct(
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewrite $resource,
        UrlRewriteFactory $urlRewriteFactory
    ) {
        $this->resource = $resource;
        $this->urlRewriteFactory = $urlRewriteFactory;
    }

    /**
     * Save rewrite.
     *
     * @param UrlRewrite $shopUrl
     * @return UrlRewrite
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(UrlRewrite $shopUrl)
    {
        try {
            $this->resource->save($shopUrl);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $shopUrl;
    }

}
