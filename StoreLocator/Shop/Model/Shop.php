<?php

namespace StoreLocator\Shop\Model;

use Magento\Framework\Model\AbstractModel;
use StoreLocator\Shop\Api\Data\ShopInterface;

/**
 * Class Shop Model
 *
 * @api
 * @method Shop setStoreId(int $storeId)
 * @method int getStoreId()
 *
 * @package StoreLocator\Shop\Model
 */
class Shop extends AbstractModel implements ShopInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'storelocator_shop';

    /**
     * @var string
     */
    protected $_eventObject = 'shop';

    public function _construct()
    {
        $this->_init(\StoreLocator\Shop\Model\ResourceModel\Shop::class);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return parent::getData(self::SHOP_ID);
    }

    /**
     * Get identifier
     *
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->getData(self::IDENTIFIER);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return parent::getData(self::SHOP_NAME);
    }

    /**
     * @return string|null
     */
    public function getImagePath(): ?string
    {
        return parent::getData(self::IMAGE_PATH);
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return parent::getData(self::ADDRESS);
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return parent::getData(self::DESCRIPTION);
    }

    /**
     * @return string|null
     */
    public function getSchedule(): ?string
    {
        return parent::getData(self::SCHEDULE);
    }

    /**
     * @return double|null
     */
    public function getLongitude()
    {
        return parent::getData(self::LONGITUDE);
    }

    /**
     * @return double|null
     */
    public function getLatitude()
    {
        return parent::getData(self::LATITUDE);
    }

    /**
     * @param int $id
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setId($id): ShopInterface
    {
        return $this->setData(self::SHOP_ID, $id);
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setIdentifier($identifier): ShopInterface
    {
        return $this->setData(self::IDENTIFIER, $identifier);
    }

    /**
     * @param string $name
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setName($name): ShopInterface
    {
        return $this->setData(self::SHOP_NAME, $name);
    }

    /**
     * @param string $image
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setImagePath($image): ShopInterface
    {
        return $this->setData(self::IMAGE_PATH, $image);
    }

    /**
     * @param string $address
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setAddress($address): ShopInterface
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * @param string $description
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setDescription($description): ShopInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }
    /**
     * @param string $schedule
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setSchedule($schedule): ShopInterface
    {
        return $this->setData(self::SCHEDULE, $schedule);
    }

    /**
     * @param string $longitude
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setLongitude($longitude)
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    /**
     * @param string $latitude
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setLatitude($latitude)
    {
        return $this->setData(self::LATITUDE, $latitude);
    }

//    /**
//     * Receive shop store ids
//     *
//     * @return int[]
//     */
//    public function getStores()
//    {
//        return $this->hasData('stores') ? $this->getData('stores') : (array)$this->getData('store_id');
//    }
}
