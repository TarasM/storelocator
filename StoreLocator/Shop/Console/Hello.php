<?php


namespace StoreLocator\Shop\Console;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Hello extends command
{
    protected function configure()
    {
        $this->setName('hello');
        $this->setDescription('saying hello');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('hello!');
    }
}
