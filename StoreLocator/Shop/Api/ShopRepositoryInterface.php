<?php

declare(strict_types=1);
namespace StoreLocator\Shop\Api;

use StoreLocator\Shop\Api\Data\ShopInterface;

/**
 * CMS page CRUD interface.
 * @api
 * @since 100.0.2
 */
interface ShopRepositoryInterface
{
    /**
     * Save shop.
     *
     * @param ShopInterface $shop
     * @return ShopInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\ShopInterface $shop);

    /**
     * Retrieve page.
     *
     * @param int $shopId
     * @return ShopInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($shopId);

    /**

     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \StoreLocator\Shop\Api\Data\ShopSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete page.
     *
     * @param ShopInterface $shop
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ShopInterface $shop);

    /**
     * Delete page by ID.
     *
     * @param int $shopId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($shopId);
}

