<?php

namespace StoreLocator\Shop\Controller\Adminhtml\Shop;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\CouldNotDeleteException;
use StoreLocator\Shop\Api\ShopRepositoryInterface;

class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'StoreLocator_Shop::shop';

    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * MassDelete constructor.
     * @param Action\Context $context
     * @param ShopRepositoryInterface $shopRepository
     */
    public function __construct(
        Action\Context $context,
        ShopRepositoryInterface $shopRepository
    ) {
        parent::__construct($context);
        $this->shopRepository = $shopRepository;
    }

    public function execute()
    {
        $selectedIds = $this->getRequest()->getParam("selected");
        foreach ($selectedIds as $shop) {
            try {
                $this->shopRepository->deleteById($shop);
            } catch (CouldNotDeleteException $ex) {
            }
        }
        $redirect = $this->resultRedirectFactory->create();
        return $redirect->setPath("*/*/");
    }
}
