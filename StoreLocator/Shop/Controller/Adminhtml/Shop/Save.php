<?php

namespace StoreLocator\Shop\Controller\Adminhtml\Shop;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use StoreLocator\Shop\Api\ShopRepositoryInterface;
use StoreLocator\Shop\Model\ImageUploader;
use StoreLocator\Shop\Model\Shop;
use StoreLocator\Shop\Model\ShopFactory;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'StoreLocator_Shop::shop';

    /**
     * @var PageFactory
     */
    private $pageFactory;
    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;
    /**
     * @var ShopFactory
     */
    private $shopFactory;

    /**
     * @var ImageUploader
     */
    private $imageUploader;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param ShopRepositoryInterface $shopRepository
     * @param ShopFactory $shopFactory
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ShopRepositoryInterface $shopRepository,
        ShopFactory $shopFactory,
        ImageUploader $imageUploader
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->shopRepository = $shopRepository;
        $this->shopFactory = $shopFactory;
        $this->imageUploader = $imageUploader;
    }

    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();

        $data = $this->getRequest()->getPostValue();

        if (empty($data['shop_id'])) {
            $data['shop_id'] = null;
        }

        /** @var Shop $shop */
        $shop = $this->shopFactory->create();

        if (isset($data['image'][0]['name'])) {
            $data['image_path'] = $data['image'][0]['url'];
            try {
                $this->imageUploader->moveFileFromTmp($data['image'][0]['name']);
                $data['image_path'] = 'http://tarasm/pub/media/storelocator/feature/' . $data['image'][0]['name'];
            } catch (LocalizedException $e) {
            }
        } elseif (empty($data['image_path'])) {
            $data['image_path'] = 'http://tarasm/pub/media/storelocator/feature/no-image.png';
        }

        // image upload

        $shop->setData($data);

        try {
            $this->shopRepository->save($shop);
        } catch (LocalizedException $e) {
        }

        return $redirect->setPath("*/*/");
    }
}
