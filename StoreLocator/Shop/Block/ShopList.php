<?php

declare(strict_types=1);
namespace StoreLocator\Shop\Block;


use StoreLocator\Shop\Model\Shop;
use StoreLocator\Shop\Model\ResourceModel\Shop\Collection as ShopCollection;
use StoreLocator\Shop\Model\ResourceModel\Shop\CollectionFactory as CollectionFactory;
use Magento\Framework\View\Element\Template;

class ShopList extends Template
{
    /**
     * @var Shop
     */
    private $shopModel;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * View constructor.
     * @param Shop $shop
     * @param CollectionFactory $collectionFactory
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Shop $shop,
        CollectionFactory $collectionFactory,
        Template\Context $context,

        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->ShopModel = $shop;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return ShopCollection
     */
    public function getShops(): ShopCollection
    {
        /** @var ShopCollection $collection */

        $collection = $this->collectionFactory->create();

        return $collection;
    }

}
