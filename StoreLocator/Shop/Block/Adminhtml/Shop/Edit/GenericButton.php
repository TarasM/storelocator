<?php


namespace StoreLocator\Shop\Block\Adminhtml\Shop\Edit;


use StoreLocator\Shop\Api\ShopRepositoryInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;

class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var ShopRepositoryInterface
     */
    protected $shopRepository;

    /**
     * @param Context $context
     * @param ShopRepositoryInterface $shopRepository
     */
    public function __construct(
        Context $context,
        ShopRepositoryInterface $shopRepository
    ) {
        $this->context = $context;
        $this->shopRepository = $shopRepository;
    }

    /**
     * Return CMS block ID
     *
     * @return int|null
     */
    public function getShopId()
    {
        try {
            return $this->shopRepository->getById(
                $this->context->getRequest()->getParam('shop_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
