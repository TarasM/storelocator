<?php

declare(strict_types=1);
namespace StoreLocator\Shop\Api\Data;

/**
 * Interface ShopInterface
 * @package StoreLocator\Shop\Api\Data
 */
interface ShopInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const SHOP_ID           = "shop_id";
    const IDENTIFIER        = 'identifier';
    const SHOP_NAME         = "shop_name";
    const DESCRIPTION       = "description";
    const IMAGE_PATH        = "image_path";
    const ADDRESS           = "address";
    const SCHEDULE          = "schedule";
    const LONGITUDE         = "longitude";
    const LATITUDE          = "latitude";
    /**#@-*/

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * Get identifier
     *
     * @return string|null
     */
    public function getIdentifier(): ?string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string|null
     */
    public function getImagePath(): ?string;

    /**
     * @return string|null
     */
    public function getAddress(): ?string;

    /**
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * @return string|null
     */
    public function getSchedule(): ?string;

    /**
     * @return double|null
     */
    public function getLongitude();

    /**
     * @return double|null
     */
    public function getLatitude();

    /**
     * @param int $id
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setId($id): ShopInterface;

    /**
     * Set identifier
     *
     * @param string $identifier
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setIdentifier($identifier): ShopInterface;

    /**
     * @param string $name
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setName($name): ShopInterface;

    /**
     * @param string $image
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setImagePath($image): ShopInterface;

    /**
     * @param string $address
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setAddress($address): ShopInterface;

    /**
     * @param string $description
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setDescription($description): ShopInterface;

    /**
     * @param string $schedule
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setSchedule($schedule): ShopInterface;

    /**
     * @param string $longitude
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setLongitude($longitude);

    /**
     * @param string $latitude
     * @return \StoreLocator\Shop\Api\Data\ShopInterface
     */
    public function setLatitude($latitude);
}
