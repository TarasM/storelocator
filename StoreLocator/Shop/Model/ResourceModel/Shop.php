<?php


namespace StoreLocator\Shop\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Shop
 * @package StoreLocator\Shop\Model
 */
class Shop extends AbstractDb
{
    public function _construct()
    {
        parent::_init("shop_table", "shop_id");
    }


}


