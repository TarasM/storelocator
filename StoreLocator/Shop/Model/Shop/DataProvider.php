<?php

namespace StoreLocator\Shop\Model\Shop;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use StoreLocator\Shop\Model\ResourceModel\Shop\CollectionFactory;

/**
 * Class DataProvider
 * @package StoreLocator\Shop\Model\Shop
 */
class DataProvider extends \Magento\Ui\DataProvider\ModifierPoolDataProvider
{
    /**
     * @var \StoreLocator\Shop\Model\ResourceModel\Shop\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $shopCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $shopCollectionFactory,
        DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->collection = $shopCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
    }

    /**
     * Get data
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getData()
    {
        //
        $storeManager =$this->storeManager;
        //$mediaUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        //

        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        /** @var \StoreLocator\Shop\Model\Shop $shop */

        foreach ($items as $shop) {
            $this->loadedData[$shop->getId()] = $shop->getData();

            //        image upload part

            $data = $shop->getData();


            if (isset($data['image'])) {
                $name = $data['image'];
                unset($data['image']);
                $shop_name = $shop['shop_name'];
                $shop_id = $shop['shop_id'];
                $data['image'][0]['name'] = $name;
                $data['image'][0]['url'] = /**$mediaUrl .*/ 'storelocator/feature/' . $name . $shop_name . $shop_id;
            }

            //        image upload part end
        }

        $data = $this->dataPersistor->get('shop');

        if (!empty($data)) {
            $shop = $this->collection->getNewEmptyItem();
            $shop->setData($data);
            $this->loadedData[$shop->getId()] = $shop->getData();
            $this->dataPersistor->clear('shop');
        }

        return $this->loadedData;
    }
}
